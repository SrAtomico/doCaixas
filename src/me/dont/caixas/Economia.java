package me.dont.caixas;

import org.bukkit.entity.Player;

public class Economia {

	public static int getMoney(Player p){
		if (Main.economia == Economy.VAULT)
			return Vault.getMoney(p);
		else if (Main.economia == Economy.MINEPAG)
			return MinePag.getMoney(p);
		else if (Main.economia == Economy.MINESHOP)
			return MineShop.getMoney(p);
		else if (Main.economia == Economy.PICOMOEDAS)
			return PMoedas.getMoney(p);
		else if (Main.economia == Economy.PLAYERPOINTS)
			return PP.getMoney(p);
		return 0;
	}
	
	public static void removeBalance(Player p, int quant){
		if (Main.economia == Economy.VAULT)
			Vault.removeBalance(p, quant);
			
		else if (Main.economia == Economy.MINEPAG)
			MinePag.removeMoney(p, quant);
			
		else if (Main.economia == Economy.MINESHOP)
			MineShop.removeMoney(p, quant);
			
		else if (Main.economia == Economy.PICOMOEDAS)
			PMoedas.removeMoney(p, quant);
			
		else if (Main.economia == Economy.PLAYERPOINTS)
			PP.removeBalance(p, quant);
			
	}
	
	public static void setEconomy(){
		if (Main.economia == Economy.VAULT)
			Vault.setupEconomy();
			
		else if (Main.economia == Economy.MINEPAG)
			MinePag.setShop();
			
		else if (Main.economia == Economy.MINESHOP)
			MineShop.setShop();
			
		else if (Main.economia == Economy.PICOMOEDAS)
			PMoedas.setPico();
			
		else if (Main.economia == Economy.PLAYERPOINTS)
			PP.setPlayerPoints();
			

	}
	
}
