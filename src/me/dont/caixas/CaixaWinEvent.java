package me.dont.caixas;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

public final class CaixaWinEvent extends Event implements Cancellable{
    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled;
    private Caixa caixa;
    private Player player;
    private ItemStack ps;
    
    public CaixaWinEvent(Caixa c, Player p, ItemStack item) {
    	caixa = c;
    	player = p;
    	ps = item;
    }

    public Caixa getCaixa() {
        return caixa;
    }
    
    public Player getPlayer(){
    	return player;
    }
    
    public ItemStack getItem(){
    	return ps;
    }
    
    public void setItem(ItemStack item){
    	ps = item;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean arg0) {
		cancelled = arg0;
	}
}
	
