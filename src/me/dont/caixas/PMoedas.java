package me.dont.caixas;

import org.bukkit.entity.Player;

import com.mrpowergamerbr.picomoedas.PicoMoedas;
import com.mrpowergamerbr.picomoedas.PicoMoedasAPI;

public class PMoedas {

	public static PicoMoedas pico = null;
	public static PicoMoedasAPI picoapi = null;
	
	public static void setPico(){
        if (Main.getPlugin(Main.class).getServer().getPluginManager().getPlugin("PicoMoedas") != null) {
        	PicoMoedas plugin = (PicoMoedas) Main.getPlugin(Main.class).getServer().getPluginManager().getPlugin("PicoMoedas");
        	pico = plugin;
        	picoapi = new PicoMoedasAPI(pico);
	        Main.economia = Economy.PICOMOEDAS;

        }
	}
	
	public static int getMoney(Player p){
		return (int) picoapi.getBalance(p).getValue();
	}
	
	public static void removeMoney(Player p, int quantia){
		picoapi.editBalance(p, quantia);
	}
}
